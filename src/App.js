import React, { Component } from "react";
import "./App.css";

class App extends Component {
  render() {
    const FlickerCards = apiComponentWrapper(Card, "/api/flicker");

    return (
      <main className="main-container">
        <h1 className="main-container__title">Flicker API cards</h1>
        <div className="card-container">
          <FlickerCards />
        </div>
      </main>
    );
  }
}

/**
 * Higher order function for wrapping a component in an image API
 * Never implemented another API, but was thinking of using Upsplash
 *
 * @param {React.Component} Comp component to render
 * @param {String} fetchUrl url to fetch from
 */
const apiComponentWrapper = (Comp, fetchUrl) => {
  return class extends Component {
    state = {
      apiResponse: []
    };

    /**
     * Fetches the API data when the component mounts.
     * This could be moved into a higher level of state
     * so that it doesn't get hit again if the component
     * gets swapped out for a separate API.
     */
    componentDidMount() {
      fetch(fetchUrl)
        .then(res => res.json())
        .then(json => {
          this.setState({ apiResponse: json });
        });
    }

    render() {
      const { apiResponse } = this.state;
      return apiResponse.map((imageJson, index) => (
        <Comp imageJson={imageJson} key={`${index}+${Date.now()}`} />
      ));
    }
  };
};

/**
 *
 * @param {Object} props.imageJson json from the api
 */
const Card = ({ imageJson }) => {
  const { src, imageUrl, authorUrl, author, tags, title } = imageJson;
  const tagsString = tags.map((tag, index, array) => {
    //Set up the array of tags to have a ','
    //unless its the last one
    let string = `${tag}, `;
    if (index === array.length - 1) {
      string = `${tag}`;
    }
    return string;
  });

  const description = `Picture was uploaded by ${author}, and is titled ${title}`;

  return (
    <section className="card-component">
      <img
        className="card-component__image"
        src={src}
        width="400"
        height="300"
        alt=""
      />
      <div className="card-component__title-section">
        <h2 className="card-component__title">
          <a className="card-component__link" href={imageUrl}>
            {title}
          </a>
        </h2>
        <p className="card-component__title-text">
          Published by{" "}
          <a className="card-component__link" href={authorUrl}>
            {author}
          </a>
        </p>
      </div>
      <p className="card-component__description">
        {description}
      </p>
      <p className="card-component__tags">{tagsString}</p>
    </section>
  );
};

export default App;
