const express = require("express");
const app = express();
const request = require("request");
const port = 2000;
let postCount = 1;

app.get("/api/flicker/", (req, res) => {
  try {
    request(
      "https://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=?&tags=tree",
      (err, body) => {
        res.json(cleanFlickerResponse(JSON.parse(body.body.slice(1, -1))));
        console.log(`Hello from flicker ${postCount++}`);
      }
    );
  } catch (error) {
    console.log("Request Failed");
  }
});

app.listen(port, () => console.log(`connected to API sever on port ${port}`));

/**
 * Takes a flicker API response and cleans its data
 * into something more usable by the react app.
 *
 * If I was going to add another API this would help to
 * standardize the data taken in by the frontend app.
 *
 * @param {JSON} json Object form the APi
 */
function cleanFlickerResponse(json) {
  const newJson = json.items.map(imageJson => {
    const {
      media: { m: src },
      tags: tagsArray,
      author: authorFirst,
      link: imageUrl,
      title,
      author_id: authorId
    } = imageJson;
    const tags = tagsArray.split(" ");
    const author = authorFirst.substring(
      authorFirst.lastIndexOf('("') + 2,
      authorFirst.lastIndexOf('")')
    );
    const authorUrl = `https://www.flickr.com/people/${authorId}/`;

    return { src, tags, author, imageUrl, title, authorUrl };
  });
  return newJson;
}
