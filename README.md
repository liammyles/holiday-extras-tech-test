# Holiday Extras Tech Test

Tech test done by Liam Myles for Holiday Extras.

This project was made using create react app.

## Running the App

The app runs on **localhost:3000** & **localhost:2000**. Ensure these ports are clear before running the app. This app was also made using **node V10.4.0**, but should work with different node versions.

All commands are from the root directory.

Start by installing the packages, this can be done with `yarn` or `npm install`.

To run the app start the server with `node server`. Then start the react app with `yarn start` or `npm run start`.
